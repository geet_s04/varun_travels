<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    //
    protected $table = 'crms_bookings';

    public $primarykey = 'b_id';
}
