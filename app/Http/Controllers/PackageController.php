<?php

namespace App\Http\Controllers;

use App\Package;
use App\Http\Controllers\Controller;
use App\PackageDetail;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $package = Package::where('category','1')->get();

        return view('event.event-editview')->with('tour_package',$package);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function show(Package $package)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $package)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Package $package)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        //
    }

    public function package_list($id)
    {
        $package_lists = PackageDetail::where('master_pack_id',$id)->get();
        $master_package = Package::where('id',$id)->first();
        return view('package_list')->with('package_lists',$package_lists)->with('master_package',$master_package);
    }

    public function package_detail($id)
    {
        $package_detail = PackageDetail::where('id',$id)->first();
        return view('package_detail')->with('package_detail',$package_detail)->with('day_wise_data', json_decode($package_detail->day_wise_detail, true));
    }
}
