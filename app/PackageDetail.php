<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageDetail extends Model
{
    //
    protected $table = 'crms_package_list';

    public $primarykey = 'id';
}
