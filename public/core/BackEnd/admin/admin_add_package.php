<?php
  session_start();
  include('inc/config.php');
  include('inc/checklogin.php');
  check_login();
  //hold logged in user session.
  $a_id = $_SESSION['a_id'];
  //register car
  
		if(isset($_POST['pack_add']))
		{
            $pack_name = $_POST['pack_name'];
            $pack_short_des = $_POST['pack_short_des'];
            $night = $_POST['night'];
            $day = $_POST['day'];
            $duration = $night.','.$day;
            $pack_type = $_POST['pack_type'];
            $pack_price = $_POST['pack_price'];
            $pack_location = $_POST['pack_location'];
            $pack_long_des = $_POST['pack_long_des'];
            $inclusion = $_POST['inclusion'];
            $exclusion = $_POST['exclusion'];
            $map = $_POST['map'];
            $day_wise = "wqerwe";
            //save car images
            $img1  = $_FILES["img1"]["name"];
            $temp_name_img1 = rand(5, 15)."_".$img1;
		    move_uploaded_file($_FILES["img1"]["tmp_name"],"../Uploads/package/".$temp_name_img1);//move uploaded image
            
            $img2  = $_FILES["img2"]["name"];
            $temp_name_img2 = rand(5, 15)."_".$img2;
		    move_uploaded_file($_FILES["img2"]["tmp_name"],"../Uploads/package/".$temp_name_img2);//move uploaded image
                        
            $img3  = $_FILES["img3"]["name"];
            $temp_name_img3 = rand(5, 15)."_".$img3;
		    move_uploaded_file($_FILES["img3"]["tmp_name"],"../Uploads/package/".$temp_name_img3);//move uploaded image
            
            $img4=$_FILES["img4"]["name"];
            $temp_name_img4 = rand(5, 15)."_".$img4;
            move_uploaded_file($_FILES["img4"]["tmp_name"],"../Uploads/package/".$temp_name_img4);
            
            $images = $temp_name_img1.",".$temp_name_img2.",".$temp_name_img3.",".$temp_name_img4;

           // echo $pack_name.",".$pack_type.",".$pack_short_des.",".$images.",".$pack_location.",".$pack_price.",".$duration.",".$pack_long_des.",".$inclusion.",".$exclusion.",".$map.",".$day_wise;
            //sql to insert captured values
            $query="INSERT INTO crms_package_list (pack_name, master_pack_id, pack_short_inf, images, pack_location, price, duration, pack_long_desc, inclusion, exclusion, map, day_wise_detail) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
            $stmt = $mysqli->prepare($query);
            $rc=$stmt->bind_param('ssssssssssss', $pack_name, $pack_type, $pack_short_des, $images, $pack_location, $pack_price, $duration, $pack_long_des, $inclusion, $exclusion, $map, $day_wise);
            $stmt->execute();

            if($stmt)
            {
                      $success = "Package Added";
                      
                      //echo "<script>toastr.success('Have Fun')</script>";
            }
            else {
              $err = "Please Try Again Or Try Later";
            }
			
			
		}
?>

<!DOCTYPE html>
<html lang="en">

<?php include("inc/head.php");?>

<body class="">
 <!--Sidebar-->
 <?php include("inc/sidebar.php");?>
  
  <div class="main-content">
    <!-- Navbar -->
   <?php include("inc/nav.php");?>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="header  pb-8 pt-5 pt-md-8" style="min-height: 300px; background-image: url(../../img/header-bg.jpg); background-size: cover; background-position: center top;">
        <span class="mask bg-gradient-default opacity-5"></span>
    </div>

    <div class="container-fluid mt--7">
        <!--Pie chart to show number of car categories-->
        <div class="row">
            <div class="card col-md-12">
                <h2 class="card-header">Add New 🏎 Package</h2>
                <div class="card-body">
                    <!--Form-->
                    <form method="post" enctype="multipart/form-data" >
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="exampleInputEmail1">Packagae Name</label>
                                <input type="text" required name="pack_name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="exampleInputEmail1">Package Short Description</label>
                                <input type="text" required name="pack_short_des" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                            </div>

                            <div class="form-group col-md-3">
                                <label for="exampleInputEmail1">Night</label>
                                <input type="text" required name="night" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                            </div>

                            <div class="form-group col-md-3">
                                <label for="exampleInputEmail1">Days</label>
                                <input type="text" required name="day" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                            </div>
                            
                            <div class="form-group col-md-6">
                                <label for="exampleInputEmail1">Package Category</label>
                                <select class="form-control" name="pack_type" id="pack_type" >
                                
                                    <option>Select Package Category</option>
                                    <?php
                                        //get all car categories
                                        $ret="SELECT * FROM crms_package ORDER BY RAND() LIMIT 10 "; 
                                        $stmt= $mysqli->prepare($ret) ;
                                        $stmt->execute() ;//ok
                                        $res=$stmt->get_result();
                                        $cnt=1;
                                        while($row=$res->fetch_object())
                                        {
                                    ?>
                                        <option value="<?php echo $row->id;?>"> <?php echo $row->name;?></option>
                                    <?php }?>
                                </select>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="exampleInputEmail1">Price</label>
                                <input type="text" required name="pack_price" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                            </div>

                            <div class="form-group col-md-3">
                                <label for="exampleInputEmail1">Location</label>
                                <input type="text" required name="pack_location" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="exampleInputEmail1">Map</label>
                                <input type="text" required name="map" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-3">
                                <label for="exampleInputEmail1">Image1</label>
                                <input type="file" required name="img1" class="form-control btn btn-outline-success" id="exampleInputEmail1" aria-describedby="emailHelp">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="exampleInputEmail1">Image2 </label>
                                <input type="file" required name="img2" class="form-control btn btn-outline-success" id="exampleInputEmail1" aria-describedby="emailHelp">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="exampleInputEmail1">Image3 </label>
                                <input type="file" required name="img3" class="form-control btn btn-outline-success" id="exampleInputEmail1" aria-describedby="emailHelp">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="exampleInputEmail1">Image4 </label>
                                <input type="file" required name="img4" class="form-control btn btn-outline-success" id="exampleInputEmail1" aria-describedby="emailHelp">
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="exampleInputEmail1">Package Long Description</label>
                                <textarea type="text" required name="pack_long_des"  class="form-control" id="long_desc" aria-describedby="emailHelp"></textarea>
                            </div>
                            
                        </div> 
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="exampleInputEmail1">Inclusion</label>
                                <textarea type="text" required name="inclusion"  class="form-control" id="inclusion" aria-describedby="emailHelp"></textarea>
                            </div>
                            
                        </div> 
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="exampleInputEmail1">Exclusion</label>
                                <textarea type="text" required name="exclusion"  class="form-control" id="exclusion" aria-describedby="emailHelp"></textarea>
                            </div>
                            
                        </div> 

                        <button type="submit" name="pack_add" class="btn btn-outline-success">Add Package</button>
                    </form>
                    <!-- ./ Form -->
                </div>    
            </div>
        </div>
      <!-- Footer -->
        <?php include("inc/footer.php");?>      
    </div>
  </div>
  <script src="//cdn.ckeditor.com/4.13.1/full/ckeditor.js"></script>
    <script type="text/javascript">
    CKEDITOR.replace('long_desc');
    CKEDITOR.replace('inclusion');
    CKEDITOR.replace('exclusion');
  </script>
  <script src="assets/js/canvasjs.min.js"></script>
  <script src="assets/js/plugins/jquery/dist/jquery.min.js"></script>
  <script src="assets/js/plugins/jquery/dist/jquery.min.js"></script>
  <script src="assets/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <!--   Optional JS   -->
  <script src="assets/js/plugins/chart.js/dist/Chart.min.js"></script>
  <script src="assets/js/plugins/chart.js/dist/Chart.extension.js"></script>
  <!--   Argon JS   -->
  <script src="assets/js/argon-dashboard.min.js?v=1.1.2"></script>
</body>

</html>