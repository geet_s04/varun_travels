<footer class="py-5">
      <div class="container">
        <div class="row align-items-center justify-content-xl-between">
          <div class="col-xl-12">
            <div class="copyright text-center text-xl-left text-muted">
              © 2021 - <?php echo date('Y');?> VarunTravelsAgency.
            </div>
          </div>
        </div>
      </div>
    </footer>
