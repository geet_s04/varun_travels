<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


Route::get('/features', function () {
    return view('features');
});


Route::get('/contact', function () {
    return view('contact');
});

Route::resource('/reserve', BookingController::class);

Route::post('/getquote', function () {
    return view('getquote');
});

Route::resource('package','PackageController');

Route::get('package_list/{id}', 'PackageController@package_list');

Route::get('package_detail/{id}', 'PackageController@package_detail');