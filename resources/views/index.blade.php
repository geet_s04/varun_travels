<!DOCTYPE html>
<html lang="en">

@include('partial.header-car-theme') 

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
<?php include('core/BackEnd/admin/inc/config.php');?>
    <!-- MOBILE MENU -->
    @include('partial.navigation-car-theme') 
	
	<!--HEADER SECTION-->
	<section>
		<div class="v2-hom-search">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
					<div class="v2-ho-se-ri">
						<h5>Gujarat's Leading Tours and Travel Agency</h5>
						<h1>Car Rentals easy now!</h1>
						<p>Plan your trip today</p>
						<div class="tourz-hom-ser v2-hom-ser">
							<!-- <ul>
								<li>
									<a href="#" class="waves-effect waves-light btn-large tourz-pop-ser-btn"><img src="core/images/car-theme/icon/2.png" alt=""> Tour</a>
								</li>
								<li>
									<a href="#" class="waves-effect waves-light btn-large tourz-pop-ser-btn"><img src="core/images/car-theme/icon/31.png" alt=""> Flight</a>
								</li>
								<li>
									<a href="#" class="waves-effect waves-light btn-large tourz-pop-ser-btn"><img src="core/images/car-theme/icon/30.png" alt=""> Car Rentals</a>
								</li>
								<li>
									<a href="#" class="waves-effect waves-light btn-large tourz-pop-ser-btn"><img src="core/images/car-theme/icon/1.png" alt=""> Hotel</a>
								</li>								
							</ul> -->
						</div>
					</div>						
					</div>	
					<div class="col-md-6">
					<div class="">
						<form class="contact__form v2-search-form" method="post" action="">
						@csrf
							<div class="alert alert-success contact__msg" style="display: none" role="alert">
								Thank you message
							</div>
							<div class="row">
								<div class="input-field col s12">
									<input type="text"  class="validate" name="name" required>
									<label>Enter your name</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s6">
									<input type="number"  class="validate" name="phone" required>
									<label>Enter your phone</label>
								</div>
								<div class="input-field col s6">
									<input type="email"  class="validate" name="email" required>
									<label>Enter your email</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s12">
									<input type="text" id="select-city" class="autocomplete" name="pickuplocation">
									<label for="select-city">Pick up location</label>
								</div>
								<div class="input-field col s12">
									<input type="text" id="select-city-1" class="autocomplete" name="droplocation">
									<label for="select-city">Dropping off location</label>
								</div>								
							</div>
							<div class="row">
								<div class="input-field col s6">
									<input type="text" id="from" name="pickdate">
									<label for="from">Pick up date</label>
								</div>
								<div class="input-field col s6">
									<select name="picktime">
										<option value="" disabled selected>Pick up time</option>
										<option value="24:00 midnight">24:00 midnight</option>
										<option value="01:00 AM">01:00 AM</option>
										<option value="02:00 AM">02:00 AM</option>
										<option value="03:00 AM">03:00 AM</option>
										<option value="04:00 AM">04:00 AM</option>
										<option value="05:00 AM">05:00 AM</option>
										<option value="06:00 AM">06:00 AM</option>
										<option value="07:00 AM">07:00 AM</option>
										<option value="08:00 AM">08:00 AM</option>
										<option value="09:00 AM">09:00 AM</option>
										<option value="10:00 AM">10:00 AM</option>
										<option value="11:00 AM">11:00 AM</option>
										<option value="12:00 noon">12:00 noon</option>
										<option value="13:00 PM">13:00 PM</option>
										<option value="14:00 PM">14:00 PM</option>
										<option value="15:00 PM">15:00 PM</option>
										<option value="16:00 PM">16:00 PM</option>
										<option value="17:00 PM">17:00 PM</option>
										<option value="18:00 PM">18:00 PM</option>
										<option value="19:00 PM">19:00 PM</option>
										<option value="20:00 PM">20:00 PM</option>
										<option value="21:00 PM">21:00 PM</option>
										<option value="22:00 PM">22:00 PM</option>
										<option value="23:00 PM">23:00 PM</option>
										<option value="24:00 midnight">06:00 AM</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s6">
									<input type="text" id="to" name="dropdate">
									<label for="to">Drop off date</label>
								</div>
								<div class="input-field col s6">
									<select name="droptime">
										<option value="" disabled selected>Drop off time</option>
										<option value="24:00 midnight">24:00 midnight</option>
										<option value="01:00 AM">01:00 AM</option>
										<option value="02:00 AM">02:00 AM</option>
										<option value="03:00 AM">03:00 AM</option>
										<option value="04:00 AM">04:00 AM</option>
										<option value="05:00 AM">05:00 AM</option>
										<option value="06:00 AM">06:00 AM</option>
										<option value="07:00 AM">07:00 AM</option>
										<option value="08:00 AM">08:00 AM</option>
										<option value="09:00 AM">09:00 AM</option>
										<option value="10:00 AM">10:00 AM</option>
										<option value="11:00 AM">11:00 AM</option>
										<option value="12:00 noon">12:00 noon</option>
										<option value="13:00 PM">13:00 PM</option>
										<option value="14:00 PM">14:00 PM</option>
										<option value="15:00 PM">15:00 PM</option>
										<option value="16:00 PM">16:00 PM</option>
										<option value="17:00 PM">17:00 PM</option>
										<option value="18:00 PM">18:00 PM</option>
										<option value="19:00 PM">19:00 PM</option>
										<option value="20:00 PM">20:00 PM</option>
										<option value="21:00 PM">21:00 PM</option>
										<option value="22:00 PM">22:00 PM</option>
										<option value="23:00 PM">23:00 PM</option>
										<option value="24:00 midnight">06:00 AM</option>
									</select>
								</div>
							</div>							
							<div class="row">
								<div class="input-field col s6">
									<select name="selectcar">
										<option value="" disabled selected>Select car type</option>
										<option value="Micro">Micro</option>
										<option value="Mini">Mini</option>
										<option value="Prime">Prime</option>
										<option value="Prime SUV">Prime SUV</option>
										<option value="Luxury Cars">Luxury Cars</option>
										<option value="Mini Van">Mini Van</option>
										<option value="Small Bus">Small Bus</option>
										<option value="Luxury Bus">Luxury Bus</option>
									</select>
								</div>
								<div class="input-field col s6">
									<select name="totalpassangers">
										<option value="" disabled selected>Total passengers</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="10">10</option>
										<option value="15">15</option>
										<option value="20">20</option>
										<option value="50">50</option>
										<option value="100">100</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s6">
									<select name="noofadults">
										<option value="" disabled selected>No of adults</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
									</select>
								</div>
								<div class="input-field col s6">
									<select name="noofchildrens">
										<option value="" disabled selected>No of childrens</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>											
									</select>
								</div>
							</div>														
							<div class="row">
								<div class="input-field col s12">
									<input type="submit" value="Book Now" class="waves-effect waves-light tourz-sear-btn v2-ser-btn">
								</div>
							</div>
						</form>
					</div>						
					</div>				
				</div>
			</div>
		</div>
	</section>
	<!--END HEADER SECTION-->
	<!--====== HOME PLACES ==========-->
	<section>
		<div class="rows pad-bot-redu tb-space">
			<div class="container">
				<!-- TITLE & DESCRIPTION -->
				<div class="spe-title">
					<h2>Top <span>Travel Packages</span></h2>
					<div class="title-line">
						<div class="tl-1"></div>
						<div class="tl-2"></div>
						<div class="tl-3"></div>
					</div>
					<p>Ahmedabad's leading Car/Bus Rental website.</p>
				</div>
				<?php $tour_packages = App\Package::where('category','1')->get(); ?>
				
			<div>
			@foreach($tour_packages as $tour_package)
				<div class="col-md-4 col-sm-6 col-xs-12 b_packages">
						<!-- OFFER BRAND 
						<div class="band"> <img src="core/images/car-theme/band.png" alt="" /> </div> -->
						<!-- IMAGE -->
						<div class="v_place_img"> <img src="core/images/car-theme/{{$tour_package->image}}" alt="Tour Booking" title="{{$tour_package->name}}" /> </div>
						<!-- TOUR TITLE & ICONS -->
						<div class="b_pack rows">
							<!-- TOUR TITLE -->
							<div class="col-md-8 col-sm-8">
								<h4><a href="{{url('package_list/'.$tour_package->id)}}">{{$tour_package->name}}</a></h4> </div>
							<!-- TOUR ICONS -->
							<div class="col-md-4 col-sm-4 pack_icon">
								<ul>
									<li>
										<a href="#"><img src="core/images/car-theme/clock.png" alt="Date" title="Tour Timing" /> </a>
									</li>
									<li>
										<a href="#"><img src="core/images/car-theme/info.png" alt="Details" title="View more details" /> </a>
									</li>
									<li>
										<a href="#"><img src="core/images/car-theme/price.png" alt="Price" title="Price" /> </a>
									</li>
									<li>
										<a href="#"><img src="core/images/car-theme/map.png" alt="Location" title="Location" /> </a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					@endforeach
					
				</div>
			</div>
		</div>
	</section>
	<!--====== HOME HOTELS ==========-->
	<section>
		<div class="rows tb-space pad-top-o pad-bot-redu">
			<div class="container">
				<!-- TITLE & DESCRIPTION -->
				<div class="spe-title">
					<h2>One <span>Way</span> </h2>
					<div class="title-line">
						<div class="tl-1"></div>
						<div class="tl-2"></div>
						<div class="tl-3"></div>
					</div>
					<p>Book your one way ride for outstation and enjoy your holidays with distinctive experience</p>
				</div>
				<!-- CITY -->
				<div class="col-md-6">
				<div class="">
				<?php 
					$curl = curl_init();

					curl_setopt_array($curl, array(
					  CURLOPT_URL => "https://academylms.tk/kareem_taxi_home.php",
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 30,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => "POST",
					  CURLOPT_POSTFIELDS => "{\n\t\"functionality\":\"retrieve_base_location\"\n}",
					  CURLOPT_HTTPHEADER => array(
						"cache-control: no-cache",
						"content-type: application/json",
						"postman-token: 1f7415ed-c78b-9ae9-0060-6866dbbfaa27"
					  ),
					));
					
					$response = curl_exec($curl);
					$err = curl_error($curl);
					$someArray = json_decode($response);
					curl_close($curl);
					
					if ($err) {
					  echo "cURL Error #:" . $err;
					} 
					
					?>
				<form class="contact__form v2-search-form" method="post" action="">
						@csrf
							<div class="alert alert-success contact__msg" style="display: none" role="alert">
								Thank you message
							</div>
							<div class="row">
								<div class="input-field col s12">
									<select name="select_base_city" id="select_base_city">
										<option value="" disabled selected>Pick up location</option>
										@foreach($someArray->city_list as $citylist)
											<option value="{{$citylist->id}}">{{$citylist->name}}</option>
										@endforeach
									</select>
								</div>
								<div class="input-field col s12">
									<select name="select_destination_city" id="select_destination_city">
										<option value="" disabled selected>Dropping off locationn</option>
										<option value="Micro">Micro</option>
										<option value="Mini">Mini</option>
										<option value="Prime">Prime</option>
										<option value="Prime SUV">Prime SUV</option>
										<option value="Luxury Cars">Luxury Cars</option>
										<option value="Mini Van">Mini Van</option>
										<option value="Small Bus">Small Bus</option>
										<option value="Luxury Bus">Luxury Bus</option>
									</select>
								</div>								
							</div>
							<div class="row">
								<div class="input-field col s6">
									<input type="text" id="from" name="pickdate">
									<label for="from">Pick up date</label>
								</div>
								<div class="input-field col s6">
									<select name="picktime">
										<option value="" disabled selected>Pick up time</option>
										<option value="24:00 midnight">24:00 midnight</option>
										<option value="01:00 AM">01:00 AM</option>
										<option value="02:00 AM">02:00 AM</option>
										<option value="03:00 AM">03:00 AM</option>
										<option value="04:00 AM">04:00 AM</option>
										<option value="05:00 AM">05:00 AM</option>
										<option value="06:00 AM">06:00 AM</option>
										<option value="07:00 AM">07:00 AM</option>
										<option value="08:00 AM">08:00 AM</option>
										<option value="09:00 AM">09:00 AM</option>
										<option value="10:00 AM">10:00 AM</option>
										<option value="11:00 AM">11:00 AM</option>
										<option value="12:00 noon">12:00 noon</option>
										<option value="13:00 PM">13:00 PM</option>
										<option value="14:00 PM">14:00 PM</option>
										<option value="15:00 PM">15:00 PM</option>
										<option value="16:00 PM">16:00 PM</option>
										<option value="17:00 PM">17:00 PM</option>
										<option value="18:00 PM">18:00 PM</option>
										<option value="19:00 PM">19:00 PM</option>
										<option value="20:00 PM">20:00 PM</option>
										<option value="21:00 PM">21:00 PM</option>
										<option value="22:00 PM">22:00 PM</option>
										<option value="23:00 PM">23:00 PM</option>
										<option value="24:00 midnight">06:00 AM</option>
									</select>
								</div>
							</div>										
							<div class="row">
								<div class="input-field col s12">
									<input type="submit" value="Get Price" class="waves-effect waves-light tourz-sear-btn v2-ser-btn">
								</div>
							</div>
						</form>
					</div>
					<!-- <a href="tour-details.html">
						<div class="tour-mig-like-com">
							<div class="tour-mig-lc-img"> <img src="core/images/car-theme/listing/home.jpg" alt=""> </div>
							<div class="tour-mig-lc-con">
								<h5>Jaipur</h5>
								<p><span>12 Packages</span> Starting from $2400</p>
							</div>
						</div>
					</a> -->
				</div>
				<div class="col-md-6">
				<table id="myTable">
					<tbody>
						<tr>
							<th>#</th>
							<th>Car Type</th>
							<th class="e_h1">Price</th>
							<th>Book</th>
						</tr>
						<tr>
							<td>1</td>
							<td class="e_h1">16.12.2016</td>
							<td class="e_h1">10.00 PM</td>
							<td><a href="booking.html" class="link-btn">Book Now</a> </td>
						</tr>
						<tr style="">
							<td>2</td>
							<td class="e_h1">16.12.2016</td>
							<td class="e_h1">10.00 PM</td>
							<td><a href="booking.html" class="link-btn">Book Now</a> </td>
						</tr>
					</tbody>
				</table>
					<!-- <a href="tour-details.html">
						<div class="tour-mig-like-com">
							<div class="tour-mig-lc-img"> <img src="core/images/car-theme/listing/home.jpg" alt=""> </div>
							<div class="tour-mig-lc-con">
								<h5>Jaipur</h5>
								<p><span>12 Packages</span> Starting from $2400</p>
							</div>
						</div>
					</a> -->
				</div>
				<!-- <div class="col-md-3">
					<a href="tour-details.html">
						<div class="tour-mig-like-com">
							<div class="tour-mig-lc-img"> <img src="core/images/car-theme/listing/home3.jpg" alt=""> </div>
							<div class="tour-mig-lc-con tour-mig-lc-con2">
								<h5>Kutch</h5>
								<p>Starting from $2400</p>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-3">
					<a href="tour-details.html">
						<div class="tour-mig-like-com">
							<div class="tour-mig-lc-img"> <img src="core/images/car-theme/listing/home2.jpg" alt=""> </div>
							<div class="tour-mig-lc-con tour-mig-lc-con2">
								<h5>Agra</h5>
								<p>Starting from $2400</p>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-3">
					<a href="tour-details.html">
						<div class="tour-mig-like-com">
							<div class="tour-mig-lc-img"> <img src="core/images/car-theme/listing/home1.jpg" alt=""> </div>
							<div class="tour-mig-lc-con tour-mig-lc-con2">
								<h5>Manali</h5>
								<p>Starting from $2400</p>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-3">
					<a href="tour-details.html">
						<div class="tour-mig-like-com">
							<div class="tour-mig-lc-img"> <img src="core/images/car-theme/listing/home4.jpg" alt=""> </div>
							<div class="tour-mig-lc-con tour-mig-lc-con2">
								<h5>Darjeeling</h5>
								<p>Starting from $2400</p>
							</div>
						</div>
					</a>
				</div> -->
			</div>
		</div>
	</section>
	<section id="vehicles" class="container">
		<div class="row tb-space pad-top-o pad-bot-redu">
		  <div class="col-md-12">
			<!-- TITLE & DESCRIPTION -->
			<div class="spe-title">
					<h2>Rent your <span>Ride</span> </h2>
					<div class="title-line">
						<div class="tl-1"></div>
						<div class="tl-2"></div>
						<div class="tl-3"></div>
					</div>
				</div>
				</div>

		  <!-- Vehicle nav start -->
		  <div class="col-md-3 vehicle-nav-row wow fadeInUp animated" data-wow-offset="100" style="visibility: visible; animation-name: fadeInUp;">
			<div id="vehicle-nav-container">
			  <ul class="vehicle-nav" style="top: -61px;">
				<?php
				//print_r($res1->fetch_object());
				$ret="SELECT * FROM crms_cars  ORDER BY car_id "; 
					$stmt= $mysqli->prepare($ret) ;
					$stmt->execute() ;//ok
					$res=$stmt->get_result();
					//$res1=$stmt->get_result();
					$cnt=1;
				while($row1=$res->fetch_object())
					{
					 $ncarid = 10;
					 $active="";
					 $carid = $row1->car_id;
					 if($ncarid > $carid){
						 $active = "active";
					 }
				?>
				  <li class="<?php echo $active; ?>"><a href="#vehicle-<?php echo $row1->car_id;?>"><?php echo $row1->car_name;?></a><span class="">&nbsp;</span></li>
					<?php } ?>
			  </ul>
			<div class="styled-select-vehicle-data">
				<select class="vehicle-data-select">
					<?php
					$stmt1= $mysqli->prepare($ret) ;
					$stmt1->execute() ;//ok
					$res1=$stmt1->get_result();
					while($row1=$res1->fetch_object())
					{ ?>
					<option value="#vehicle-<?php echo $row1->car_id;?>"><?php echo $row1->car_name;?></option>
					<?php } ?>
				</select>
					</div></div>

			  <div class="vehicle-nav-control">
				  <a class="vehicle-nav-scroll" data-direction="up" href="#"><i class="fa fa-chevron-up"></i></a>
				  <a class="vehicle-nav-scroll" data-direction="down" href="#"><i class="fa fa-chevron-down"></i></a>
			  </div>

		  </div>
		  <!-- Vehicle nav end -->

		  <!-- Vehicle 1 data start -->
		  <?php
					$stmt2= $mysqli->prepare($ret) ;
					$stmt2->execute() ;//ok
					$res2=$stmt2->get_result();
					while($row2=$res2->fetch_object())
					{ ?>
					
		  <div class="vehicle-data" id="vehicle-<?php echo $row2->car_id;?>" >
			<div class="col-md-6 " data-wow-offset="100" >
			  <div class="vehicle-img">
				<img class="img-responsive" src="core/BackEnd/Uploads/Cars/<?php echo $row2->exterior_img;?>" alt="Vehicle">
			  </div>
			</div>
			<div class="col-md-3" data-wow-offset="200">
			  <div class="vehicle-price">₹ <?php echo $row2->car_price;?> <span class="info">rent / k.m</span></div>
			  <table class="table vehicle-features">
				<tbody><tr>
				  <td>Category</td>
				  <td><?php echo $row2->car_type;?></td>
				</tr>
				<tr>
				  <td>Reg No</td>
				  <td><?php echo $row2->car_regno;?></td>
				</tr>
				<tr>
				  <td>Status</td>
				  <td><?php if($row2->car_status =='Hired'){echo "<span class='badge badge-danger'>Hired</span>";}else{echo"<span class='badge badge-success'>Available</span>";}?></td>
				</tr>
			  </tbody>
			</table>
			  <a href="{{ url('/reserve') }}" class="reserve-button scroll-to"><span class="fa fa-calendar"></span> Reserve now</a>
			</div>
		  </div>
		  <?php } ?>
		  <!-- Vehicle 1 data end -->

		  <!-- Vehicle 2 data start -->
		  

		</div>
	  </section>
	<!--====== SECTION: FREE CONSULTANT ==========-->
	<section>
		<div class="tb-space tour-consul">
			<div class="container">
				<div class="col-md-6"> <span class="to-con-1">Get in touch with us</span>
					<h2>For Corporate Events</h2>
					<p>Get best travel deals for hotels, holidays, trains.Donec eget odio a nunc lacinia rhoncus. 
						Vivamus dignissim magna quis nisl fermentum porttitor. Maecenas venenatis mattis dui at pharetra.
						Nullam rutrum odio eu viverra accumsan. Nam orci elit, scelerisque eget dui non, sagittis tristique metus.
						Fusce nec nulla dapibus, fringilla nulla eu, vehicula odio. </p>
						<span class="to-con-2">Help line: +91 9998677923</span>
					<div class="ho-con-cont"><a href="contact.html" class="to-con-4 link-btn">Contact Now</a> </div>
				</div>
				<div class="col-md-6 ho-vid"> <img src="core/images/car-theme/video.png" alt=""> </div>
			</div>
		</div>
	</section>
	<!--====== EVENTS ==========-->
	{{-- <section>
		<div class="rows tb-space">
			<div class="container events events-1" id="inner-page-title">
				<!-- TITLE & DESCRIPTION -->
				<div class="spe-title">
					<h2>Top <span>Events</span> in this month</h2>
					<div class="title-line">
						<div class="tl-1"></div>
						<div class="tl-2"></div>
						<div class="tl-3"></div>
					</div>
					<p>Gujarats's leading tour and travels Booking website,Over 30,000 packages worldwide. Book travel packages and enjoy your holidays with distinctive experience</p>
				</div>
				<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search Event Name.." title="Type in a name">
				<table id="myTable">
					<tbody>
						<tr>
							<th>#</th>
							<th>Event Name</th>
							<th class="e_h1">Date</th>
							<th class="e_h1">Time</th>
							<th class="e_h1">Location</th>
							<th>Book</th>
						</tr>
						<tr>
							<td>1</td>
							<td><img src="core/images/car-theme/iplace-1.jpg" alt="" /><a href="hotels-list.html" class="events-title">Taj Mahal,Agra, India</a> </td>
							<td class="e_h1">16.12.2016</td>
							<td class="e_h1">10.00 PM</td>
							<td class="e_h1">Australia</td>
							<td><a href="booking.html" class="link-btn">Book Now</a> </td>
						</tr>
						<tr>
							<td>2</td>
							<td><img src="core/images/car-theme/iplace-2.jpg" alt="" /><a href="hotels-list.html" class="events-title">Salesforce Summer, Dubai</a> </td>
							<td class="e_h1">16.12.2016</td>
							<td class="e_h1">10.00 PM</td>
							<td class="e_h1">Dubai</td>
							<td><a href="booking.html" class="link-btn">Book Now</a> </td>
						</tr>
						<tr>
							<td>3</td>
							<td><img src="core/images/car-theme/iplace-3.jpg" alt="" /><a href="hotels-list.html" class="events-title">God Towers, TOKYO, JAPAN</a> </td>
							<td class="e_h1">16.12.2016</td>
							<td class="e_h1">10.00 PM</td>
							<td class="e_h1">JAPAN</td>
							<td><a href="booking.html" class="link-btn">Book Now</a> </td>
						</tr>
						<tr>
							<td>4</td>
							<td><img src="core/images/car-theme/iplace-4.jpg" alt="" /><a href="hotels-list.html" class="events-title">TOUR DE ROMANDIE, Switzerland</a> </td>
							<td class="e_h1">16.12.2016</td>
							<td class="e_h1">10.00 PM</td>
							<td class="e_h1">Switzerland</td>
							<td><a href="booking.html" class="link-btn">Book Now</a> </td>
						</tr>
						<tr>
							<td>5</td>
							<td><img src="core/images/car-theme/iplace-5.jpg" alt="" /><a href="hotels-list.html" class="events-title">TOUR DE POLOGNE, Poland</a> </td>
							<td class="e_h1">16.12.2016</td>
							<td class="e_h1">10.00 PM</td>
							<td class="e_h1">Poland</td>
							<td><a href="booking.html" class="link-btn">Book Now</a> </td>
						</tr>
						<tr>
							<td>6</td>
							<td><img src="core/images/car-theme/iplace-6.jpg" alt="" /><a href="hotels-list.html" class="events-title">Future of Marketing,Sydney, Australia</a> </td>
							<td class="e_h1">16.12.2016</td>
							<td class="e_h1">10.00 PM</td>
							<td class="e_h1">Australia</td>
							<td><a href="booking.html" class="link-btn">Book Now</a> </td>
						</tr>
						<tr>
							<td>7</td>
							<td><img src="core/images/car-theme/iplace-7.jpg" alt="" /><a href="hotels-list.html" class="events-title">Eiffel Tower, Paris</a> </td>
							<td class="e_h1">16.12.2016</td>
							<td class="e_h1">10.00 PM</td>
							<td class="e_h1">France</td>
							<td><a href="booking.html" class="link-btn">Book Now</a> </td>
						</tr>
						<tr>
							<td>8</td>
							<td><img src="core/images/car-theme/iplace-8.jpg" alt="" /><a href="hotels-list.html" class="events-title">PARIS - ROUBAIX, England</a> </td>
							<td class="e_h1">16.12.2016</td>
							<td class="e_h1">10.00 PM</td>
							<td class="e_h1">England</td>
							<td><a href="booking.html" class="link-btn">Book Now</a> </td>
						</tr>
						<tr>
							<td>9</td>
							<td><img src="core/images/car-theme/iplace-9.jpg" alt="" /><a href="hotels-list.html" class="events-title">Dubai Beach Resort, Dubai</a> </td>
							<td class="e_h1">16.12.2016</td>
							<td class="e_h1">10.00 PM</td>
							<td class="e_h1">Dubai</td>
							<td><a href="booking.html" class="link-btn">Book Now</a> </td>
						</tr>
						<tr>
							<td>10</td>
							<td><img src="core/images/car-theme/iplace-4.jpg" alt="" /><a href="hotels-list.html" class="events-title">TOUR DE POLOGNE, Poland</a> </td>
							<td class="e_h1">16.12.2016</td>
							<td class="e_h1">10.00 PM</td>
							<td class="e_h1">Poland</td>
							<td><a href="booking.html" class="link-btn">Book Now</a> </td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</section> --}}
	<!--====== POPULAR TOUR PLACES ==========-->
	{{-- <section>
		<div class="rows pla pad-bot-redu tb-space">
			<div class="pla1 p-home container">
				<!-- TITLE & DESCRIPTION -->
				<div class="spe-title spe-title-1">
					<h2>Top <span>Sight Seeing</span> in this month</h2>
					<div class="title-line">
						<div class="tl-1"></div>
						<div class="tl-2"></div>
						<div class="tl-3"></div>
					</div>
					<p>World's leading tour and travels Booking website,Over 30,000 packages worldwide. Book travel packages and enjoy your holidays with distinctive experience</p>
				</div>
				<div class="popu-places-home">
					<!-- POPULAR PLACES 1 -->
					<div class="col-md-6 col-sm-6 col-xs-12 place">
						<div class="col-md-6 col-sm-12 col-xs-12"> <img src="core/images/car-theme/place2.jpg" alt="" /> </div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<h3><span>Honeymoon Package</span> 7 Days / 6 Nights</h3>
							<p>lorem ipsum simplelorem ipsum simplelorem ipsum simplelorem ipsum simple</p> <a href="tour-details.html" class="link-btn">more info</a> </div>
					</div>
					<!-- POPULAR PLACES 2 -->
					<div class="col-md-6 col-sm-6 col-xs-12 place">
						<div class="col-md-6 col-sm-12 col-xs-12"> <img src="core/images/car-theme/place1.jpg" alt="" /> </div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<h3><span>Family package</span> 14 Days / 13 Nights</h3>
							<p>lorem ipsum simplelorem ipsum simplelorem ipsum simplelorem ipsum simple</p> <a href="tour-details.html" class="link-btn">more info</a> </div>
					</div>
				</div>
				<div class="popu-places-home">
					<!-- POPULAR PLACES 3 -->
					<div class="col-md-6 col-sm-6 col-xs-12 place">
						<div class="col-md-6 col-sm-12 col-xs-12"> <img src="core/images/car-theme/place3.jpg" alt="" /> </div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<h3><span>Weekend Package </span> 3 Days / 2 Nights</h3>
							<p>lorem ipsum simplelorem ipsum simplelorem ipsum simplelorem ipsum simple</p> <a href="tour-details.html" class="link-btn">more info</a> </div>
					</div>
					<!-- POPULAR PLACES 4 -->
					<div class="col-md-6 col-sm-6 col-xs-12 place">
						<div class="col-md-6 col-sm-12 col-xs-12"> <img src="core/images/car-theme/place4.jpg" alt="" /> </div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<h3><span>Group Package</span> 10 Days / 9 Nights</h3>
							<p>lorem ipsum simplelorem ipsum simplelorem ipsum simplelorem ipsum simple</p> <a href="tour-details.html" class="link-btn">more info</a> </div>
					</div>
				</div>
			</div>
		</div>
	</section> --}}
	<!--====== REQUEST A QUOTE ==========-->
	{{-- <section>
		<div class="ho-popu tb-space pad-bot-redu">
			<div class="rows container">
				<!-- TITLE & DESCRIPTION -->
				<div class="spe-title">
					<h2>Top <span>Branding</span> for this month</h2>
					<div class="title-line">
						<div class="tl-1"></div>
						<div class="tl-2"></div>
						<div class="tl-3"></div>
					</div>
					<p>World's leading tour and travels Booking website,Over 30,000 packages worldwide. Book travel packages and enjoy your holidays with distinctive experience</p>
				</div>
				<div class="ho-popu-bod">
					<div class="col-md-4">
						<div class="hot-page2-hom-pre-head">
							<h4>Top Branding <span>Hotels</span></h4> </div>
						<div class="hot-page2-hom-pre">
							<ul>
								<!--LISTINGS-->
								<li>
									<a href="hotels-list.html">
										<div class="hot-page2-hom-pre-1"> <img src="core/images/car-theme/hotels/1.jpg" alt=""> </div>
										<div class="hot-page2-hom-pre-2">
											<h5>Taaj Club House</h5> <span>City: illunois, United States</span> </div>
										<div class="hot-page2-hom-pre-3"> <span>4.5</span> </div>
									</a>
								</li>
								<!--LISTINGS-->
								<li>
									<a href="hotels-list.html">
										<div class="hot-page2-hom-pre-1"> <img src="core/images/car-theme/hotels/2.jpg" alt=""> </div>
										<div class="hot-page2-hom-pre-2">
											<h5>Universal luxury Grand Hotel</h5> <span>City: Rio,Brazil</span> </div>
										<div class="hot-page2-hom-pre-3"> <span>4.2</span> </div>
									</a>
								</li>
								<!--LISTINGS-->
								<li>
									<a href="hotels-list.html">
										<div class="hot-page2-hom-pre-1"> <img src="core/images/car-theme/hotels/3.jpg" alt=""> </div>
										<div class="hot-page2-hom-pre-2">
											<h5>Barcelona Grand Pales</h5> <span>City: Chennai,India</span> </div>
										<div class="hot-page2-hom-pre-3"> <span>5.0</span> </div>
									</a>
								</li>
								<!--LISTINGS-->
								<li>
									<a href="hotels-list.html">
										<div class="hot-page2-hom-pre-1"> <img src="core/images/car-theme/hotels/4.jpg" alt=""> </div>
										<div class="hot-page2-hom-pre-2">
											<h5>Lake Palace view Hotel</h5> <span>City: Beijing,China</span> </div>
										<div class="hot-page2-hom-pre-3"> <span>2.5</span> </div>
									</a>
								</li>
								<!--LISTINGS-->
								<li>
									<a href="hotels-list.html">
										<div class="hot-page2-hom-pre-1"> <img src="core/images/car-theme/hotels/8.jpg" alt=""> </div>
										<div class="hot-page2-hom-pre-2">
											<h5>First Class Grandd Hotel</h5> <span>City: Berlin,Germany</span> </div>
										<div class="hot-page2-hom-pre-3"> <span>4.0</span> </div>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-md-4">
						<div class="hot-page2-hom-pre-head">
							<h4>Top Branding <span>Packages</span></h4> </div>
						<div class="hot-page2-hom-pre">
							<ul>
								<!--LISTINGS-->
								<li>
									<a href="hotels-list.html">
										<div class="hot-page2-hom-pre-1"> <img src="core/images/car-theme/trends/1.jpg" alt=""> </div>
										<div class="hot-page2-hom-pre-2">
											<h5>Family Package Luxury</h5> <span>Duration: 7 Days and 6 Nights</span> </div>
										<div class="hot-page2-hom-pre-3"> <span>4.1</span> </div>
									</a>
								</li>
								<!--LISTINGS-->
								<li>
									<a href="hotels-list.html">
										<div class="hot-page2-hom-pre-1"> <img src="core/images/car-theme/trends/2.jpg" alt=""> </div>
										<div class="hot-page2-hom-pre-2">
											<h5>Honeymoon Package Luxury</h5> <span>Duration: 14 Days and 13 Nights</span> </div>
										<div class="hot-page2-hom-pre-3"> <span>4.4</span> </div>
									</a>
								</li>
								<!--LISTINGS-->
								<li>
									<a href="hotels-list.html">
										<div class="hot-page2-hom-pre-1"> <img src="core/images/car-theme/trends/3.jpg" alt=""> </div>
										<div class="hot-page2-hom-pre-2">
											<h5>Group Package Luxury</h5> <span>Duration: 28 Days and 29 Nights</span> </div>
										<div class="hot-page2-hom-pre-3"> <span>3.0</span> </div>
									</a>
								</li>
								<!--LISTINGS-->
								<li>
									<a href="hotels-list.html">
										<div class="hot-page2-hom-pre-1"> <img src="core/images/car-theme/trends/4.jpg" alt=""> </div>
										<div class="hot-page2-hom-pre-2">
											<h5>Regular Package Luxury</h5> <span>Duration: 12 Days and 11 Nights</span> </div>
										<div class="hot-page2-hom-pre-3"> <span>3.5</span> </div>
									</a>
								</li>
								<!--LISTINGS-->
								<li>
									<a href="hotels-list.html">
										<div class="hot-page2-hom-pre-1"> <img src="core/images/car-theme/trends/1.jpg" alt=""> </div>
										<div class="hot-page2-hom-pre-2">
											<h5>Custom Package Luxury</h5> <span>Duration: 10 Days and 10 Nights</span> </div>
										<div class="hot-page2-hom-pre-3"> <span>5.0</span> </div>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-md-4">
						<div class="hot-page2-hom-pre-head">
							<h4>Top Branding <span>Reviewers</span></h4> </div>
						<div class="hot-page2-hom-pre">
							<ul>
								<!--LISTINGS-->
								<li>
									<a href="hotels-list.html">
										<div class="hot-page2-hom-pre-1"> <img src="core/images/car-theme/reviewer/1.jpg" alt=""> </div>
										<div class="hot-page2-hom-pre-2">
											<h5>Christopher</h5> <span>No of Reviews: 620, City: illunois</span> </div>
										<div class="hot-page2-hom-pre-3"> <i class="fa fa-hand-o-right" aria-hidden="true"></i> </div>
									</a>
								</li>
								<!--LISTINGS-->
								<li>
									<a href="hotels-list.html">
										<div class="hot-page2-hom-pre-1"> <img src="core/images/car-theme/reviewer/2.png" alt=""> </div>
										<div class="hot-page2-hom-pre-2">
											<h5>Matthew</h5> <span>No of Reviews: 48, City: Rio</span> </div>
										<div class="hot-page2-hom-pre-3"> <i class="fa fa-hand-o-right" aria-hidden="true"></i> </div>
									</a>
								</li>
								<!--LISTINGS-->
								<li>
									<a href="hotels-list.html">
										<div class="hot-page2-hom-pre-1"> <img src="core/images/car-theme/reviewer/3.jpg" alt=""> </div>
										<div class="hot-page2-hom-pre-2">
											<h5>Stephanie</h5> <span>No of Reviews: 560, City: Chennai</span> </div>
										<div class="hot-page2-hom-pre-3"> <i class="fa fa-hand-o-right" aria-hidden="true"></i> </div>
									</a>
								</li>
								<!--LISTINGS-->
								<li>
									<a href="hotels-list.html">
										<div class="hot-page2-hom-pre-1"> <img src="core/images/car-theme/reviewer/4.jpg" alt=""> </div>
										<div class="hot-page2-hom-pre-2">
											<h5>Robert</h5> <span>No of Reviews: 920, City: Beijing</span> </div>
										<div class="hot-page2-hom-pre-3"> <i class="fa fa-hand-o-right" aria-hidden="true"></i> </div>
									</a>
								</li>
								<!--LISTINGS-->
								<li>
									<a href="hotels-list.html">
										<div class="hot-page2-hom-pre-1"> <img src="core/images/car-theme/reviewer/5.jpg" alt=""> </div>
										<div class="hot-page2-hom-pre-2">
											<h5>Danielle</h5> <span>No of Reviews: 768, City: Berlin</span> </div>
										<div class="hot-page2-hom-pre-3"> <i class="fa fa-hand-o-right" aria-hidden="true"></i> </div>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> --}}
	<!--====== REQUEST A QUOTE ==========-->
	{{-- <section>
		<div class="foot-mob-sec tb-space">
			<div class="rows container">
				<!-- FAMILY IMAGE(YOU CAN USE ANY PNG IMAGE) -->
				<div class="col-md-6 col-sm-6 col-xs-12 family"> <img src="core/images/car-theme/mobile.png" alt="" /> </div>
				<!-- REQUEST A QUOTE -->
				<div class="col-md-6 col-sm-6 col-xs-12">
					<!-- THANK YOU MESSAGE -->
					<div class="foot-mob-app">
						<h2>Have you tried our mobile app?</h2>
						<p>World's leading tour and travels Booking website,Over 30,000 packages worldwide. Book travel packages and enjoy your holidays with distinctive experience</p>
						<ul>
							<li><i class="fa fa-check" aria-hidden="true"></i> Easy Hotel Booking</li>
							<li><i class="fa fa-check" aria-hidden="true"></i> Tour and Travel Packages</li>
							<li><i class="fa fa-check" aria-hidden="true"></i> Package Reviews and Ratings</li>
							<li><i class="fa fa-check" aria-hidden="true"></i> Manage your Bookings, Enquiry and Reviews</li>
						</ul>
						<a href="#"><img src="core/images/car-theme/android.png" alt=""> </a>
						<a href="#"><img src="core/images/car-theme/apple.png" alt=""> </a>
					</div>
				</div>
			</div>
		</div>
	</section> --}}
	<!--====== REQUEST A QUOTE ==========-->
	<!--<section>
        <div class="form tb-space">
            <div class="rows container">
                <div class="spe-title">
                    <h2>Book your <span>favourite Package</span> Now!</h2>
                    <div class="title-line">
                        <div class="tl-1"></div>
                        <div class="tl-2"></div>
                        <div class="tl-3"></div>
                    </div>
                    <p>World's leading tour and travels Booking website,Over 30,000 packages worldwide. Book travel packages and enjoy your holidays with distinctive experience</p>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form_1">
                    <div class="succ_mess">Thank you for contacting us we will get back to you soon.</div>
                    <form id="home_form" name="home_form" action="mail/send.php">
                        <ul>
                            <li>
                                <input type="text" name="ename" value="" id="ename" placeholder="Name" required>
                            </li>
                            <li>
                                <input type="tel" name="emobile" value="" id="emobile" placeholder="Mobile" required>
                            </li>
                            <li>
                                <input type="email" name="eemail" value="" id="eemail" placeholder="Email id" required>
                            </li>
                            <li>
                                <input type="text" name="esubject" value="" id="esubject" placeholder="Subject" required>
                            </li>
                            <li>
                                <input type="text" name="ecity" value="" id="ecity" placeholder="City" required>
                            </li>
                            <li>
                                <input type="text" name="ecount" value="" id="ecount" placeholder="Country" required>
                            </li>
                            <li>
                                <textarea name="emess" cols="40" rows="3" id="text-comment" placeholder="Enter your message"></textarea>
                            </li>
                            <li>
                                <input type="submit" value="Submit" id="send_button">
                            </li>
                        </ul>
                    </form>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 family">
                    <img src="core/images/car-theme/family.png" alt="" />
                </div>
            </div>
        </div>
    </section>-->
	<!--====== TIPS BEFORE TRAVEL ==========-->
	<section>
		<div class="rows tips tips-home tb-space home_title">
			<div class="container tips_1">
				<!-- TIPS BEFORE TRAVEL -->
				<div class="col-md-4 col-sm-6 col-xs-12">
					<h3>Tips Before Travel</h3>
					<div class="tips_left tips_left_1">
						<h5>Bring copies of your passport</h5>
						<p>Aliquam pretium id justo eget tristique. Aenean feugiat vestibulum blandit.</p>
					</div>
					<div class="tips_left tips_left_2">
						<h5>Register with your embassy</h5>
						<p>Mauris efficitur, ante sit amet rhoncus malesuada, orci justo sollicitudin.</p>
					</div>
					<div class="tips_left tips_left_3">
						<h5>Always have local cash</h5>
						<p>Donec et placerat ante. Etiam et velit in massa. </p>
					</div>
				</div>
				<!-- CUSTOMER TESTIMONIALS -->
				<div class="col-md-8 col-sm-6 col-xs-12 testi-2">
					<!-- TESTIMONIAL TITLE -->
					<h3>Customer Testimonials</h3>
					<div class="testi">
						<h4>John William</h4>
						<p>Ut sed sem quis magna ultricies lacinia et sed tortor. Ut non tincidunt nisi, non elementum lorem. Aliquam gravida sodales</p> <address>Illinois, United States of America</address> </div>
					<!-- ARRANGEMENTS & HELPS -->
					<h3>Arrangement & Helps</h3>
					<div class="arrange">
						<ul>
							<!-- LOCATION MANAGER -->
							<li>
								<a href="#"><img src="core/images/car-theme/Location-Manager.png" alt=""> </a>
							</li>
							<!-- PRIVATE GUIDE -->
							<li>
								<a href="#"><img src="core/images/car-theme/Private-Guide.png" alt=""> </a>
							</li>
							<!-- ARRANGEMENTS -->
							<li>
								<a href="#"><img src="core/images/car-theme/Arrangements.png" alt=""> </a>
							</li>
							<!-- EVENT ACTIVITIES -->
							<li>
								<a href="#"><img src="core/images/car-theme/Events-Activities.png" alt=""> </a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- ================ start footer Area ================= -->
    @include('partial.footer-car-theme') 
<!-- ================ End footer Area ================= -->
	<!--========= Scripts ===========-->
	<script src="core/js/car-theme/jquery-latest.min.js"></script>
	<script src="core/js/car-theme/jquery-ui.js"></script>
	<script src="core/js/car-theme/bootstrap.js"></script>       
	<!--[if !(gte IE 8)]><!-->
            <script src="core/js/car-theme/wow.min.js"></script>
            <script>
                // Initialize WOW
                //-------------------------------------------------------------
                new WOW({mobile: false}).init();
            </script>
        <!--<![endif]-->
	<script src="core/js/car-theme/materialize.min.js"></script>
	<script src="core/js/car-theme/mail.js"></script>
	<script src="core/js/car-theme/custom.js"></script>
</body>
</html>