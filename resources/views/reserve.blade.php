
<!DOCTYPE html>
<html lang="en">
@include('partial.header-car-theme')
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- MOBILE MENU -->
    <!--HEADER SECTION-->
    @include('partial.navigation-car-theme') 
    <!--END HEADER SECTION-->
	
	<!--DASHBOARD-->
	<section>
		<div class="tr-register">
			<div class="tr-regi-form v2-search-form">
				<h4>Booking your <span>Ride</span></h4>
				<p>No additional charges.</p>
				<form class="contact__form" method="post" action="">
                    @csrf
							<div class="alert alert-success contact__msg" style="display: none" role="alert">
								Thank you for arranging a wonderful trip for us! Our team will contact you shortly!
							</div>
							<div class="row">
								<div class="input-field col s12">
									<input type="text"  class="validate" name="name" required>
									<label>Enter your name</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s6">
									<input type="number"  class="validate" name="phone" required>
									<label>Enter your phone</label>
								</div>
								<div class="input-field col s6">
									<input type="email"  class="validate" name="email" required>
									<label>Enter your email</label>
								</div>
							</div>
                            
							<div class="row">
								<div class="input-field col s12">
									<input type="text" id="select-city" class="autocomplete" name="pickuplocation">
									<label for="select-city">Pick up location</label>
								</div>
								<div class="input-field col s12">
									<input type="text" id="select-city-1" class="autocomplete" name="droplocation">
									<label for="select-city">Dropping off location</label>
								</div>								
							</div>
							<div class="row">
								<div class="input-field col s6">
									<input type="text" id="from" name="arrival" readonly>
									<label for="from">Arrival Date</label>
								</div>
								<div class="input-field col s6">
									<input type="text" id="to" name="departure" readonly>
									<label for="to">Departure Date</label>
								</div>
							</div>				
							<div class="row">
								<div class="input-field col s12">
									<input type="submit" id="getquote" value="Get Quote" class="waves-effect waves-light tourz-sear-btn v2-ser-btn">
								</div>
							</div>
						</form>
			</div>
            
            <div id="quoteresult">
            </div>
		</div>
	</section>
	<!--END DASHBOARD-->
	<!--====== TIPS BEFORE TRAVEL ==========-->
	<section>
		<div class="rows tips tips-home tb-space home_title">
			<div class="container tips_1">
				<!-- TIPS BEFORE TRAVEL -->
				<div class="col-md-4 col-sm-6 col-xs-12">
					<h3>Tips Before Travel</h3>
					<div class="tips_left tips_left_1">
						<h5>Bring copies of your passport</h5>
						<p>Aliquam pretium id justo eget tristique. Aenean feugiat vestibulum blandit.</p>
					</div>
					<div class="tips_left tips_left_2">
						<h5>Register with your embassy</h5>
						<p>Mauris efficitur, ante sit amet rhoncus malesuada, orci justo sollicitudin.</p>
					</div>
					<div class="tips_left tips_left_3">
						<h5>Always have local cash</h5>
						<p>Donec et placerat ante. Etiam et velit in massa. </p>
					</div>
				</div>
				<!-- CUSTOMER TESTIMONIALS -->
				<div class="col-md-8 col-sm-6 col-xs-12 testi-2">
					<!-- TESTIMONIAL TITLE -->
					<h3>Customer Testimonials</h3>
					<div class="testi">
						<h4>John William</h4>
						<p>Ut sed sem quis magna ultricies lacinia et sed tortor. Ut non tincidunt nisi, non elementum lorem. Aliquam gravida sodales</p> <address>Illinois, United States of America</address> </div>
					<!-- ARRANGEMENTS & HELPS -->
					<h3>Arrangement & Helps</h3>
					<div class="arrange">
						<ul>
							<!-- LOCATION MANAGER -->
							<li>
								<a href="#"><img src="images/Location-Manager.png" alt=""> </a>
							</li>
							<!-- PRIVATE GUIDE -->
							<li>
								<a href="#"><img src="images/Private-Guide.png" alt=""> </a>
							</li>
							<!-- ARRANGEMENTS -->
							<li>
								<a href="#"><img src="images/Arrangements.png" alt=""> </a>
							</li>
							<!-- EVENT ACTIVITIES -->
							<li>
								<a href="#"><img src="images/Events-Activities.png" alt=""> </a>
							</li>
						</ul>
					</div>
				</div>
			</div>
	</section>

	<!-- ================ start footer Area ================= -->
    @include('partial.footer-car-theme') 
<!-- ================ End footer Area ================= -->
	<!--========= Scripts ===========-->
	<script src="core/js/car-theme/jquery-latest.min.js"></script>
	<script src="core/js/car-theme/jquery-ui.js"></script>
	<script src="core/js/car-theme/bootstrap.js"></script>       
	<!--[if !(gte IE 8)]><!-->
            <script src="core/js/car-theme/wow.min.js"></script>
            <script>
                // Initialize WOW
                //-------------------------------------------------------------
                new WOW({mobile: false}).init();
            </script>
        <!--<![endif]-->
	<script src="core/js/car-theme/materialize.min.js"></script>
	<script src="core/js/car-theme/mail.js"></script>
	<script src="core/js/car-theme/custom.js"></script>
    <script>
    $(document).ready(function(){
        $("#getquote").click(function(){
            debugger;
           $.ajax({
            url: "getquote",
            type: "POST",
            data: {
                "_token": "{{ csrf_token() }}",
                "from": $("#select-city").val(),
                "to": $("#select-city-1").val()
            },
            cache: false,
            success: function(dataResult) {
                console.log(dataResult);
                debugger;
                $(".v2-search-form").hide();
                $("#quoteresult").html(dataResult);
            }
        });
        });
    });
        
    </script>
</body>


<!-- Mirrored from rn53themes.net/themes/demo/travelz/booking-car-rentals.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 31 Dec 2019 22:02:49 GMT -->
</html>