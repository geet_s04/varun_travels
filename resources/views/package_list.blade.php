
<!DOCTYPE html>
<html lang="en">
@include('partial.header-car-theme')
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- MOBILE MENU -->
    <!--HEADER SECTION-->
    @include('partial.navigation-car-theme') 
    <!--END HEADER SECTION-->
	
	<!--Banner-->
	<section>
        <div class="rows inner_banner inner_banner_5">
            <div class="container">
                <h2><span>{{$master_package['name']}}-</span> Top Family Packages In The World</h2><ul><li><a href="#inner-page-title">Home</a></li><li><i class="fa fa-angle-right" aria-hidden="true"></i> </li><li><a href="" class="bread-acti">{{$master_package['name']}}</a></li></ul>
                <p>{{$master_package['description']}}</p>
            </div>
        </div>
    </section>

    <section>
        <div class="rows inn-page-bg com-colo">
            <div class="container inn-page-con-bg tb-space pad-bot-redu-5" id="inner-page-title">
                <!-- TITLE & DESCRIPTION -->
                <div class="spe-title col-md-12">
                    <h2>{{$master_package['name']}} <span> packages</span></h2>
                    <div class="title-line">
                        <div class="tl-1"></div>
                        <div class="tl-2"></div>
                        <div class="tl-3"></div>
                    </div>
                    <p>World's leading tour and travels Booking website,Over 30,000 packages worldwide. Book travel packages and enjoy your holidays with distinctive experience</p>
                </div>
                @foreach($package_lists as $package_list)
                <?php $str_arr = explode (",", $package_list->duration); 
                ?>
                <!--===== PLACES ======-->
                <div class="rows p2_2">
                    <div class="col-md-6 col-sm-6 col-xs-12 p2_1">
                        <div class="band"><img src="images/band.png" alt="">
                        </div>
                        <img src="{{url('core/images/packages/iplace-2.jpg')}}" alt="">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 p2">
                        <h3>{{$package_list->pack_name}} {{$str_arr[0]}} Night/{{$str_arr[1]}} Days <span><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i></span></h3>
                        <p>{{$package_list->pack_short_inf}}</p>
                        <div class="ticket">
                            <ul>
                                <li>Booking Open</li>
                            </ul>
                        </div>
                        <div class="featur">
                            <h4>Package Locations</h4>
                            <ul>
                                <li>{{$package_list->pack_location}}</li>
                                <!-- <li>8 Breakfast, 3 Dinners</li>
                                <li>First class Sightseeing</li>
                                <li>Lorem ipsum</li> -->
                            </ul>
                        </div>
                        <div class="p2_book">
                            <ul>
                                <li><a href="booking.html" class="link-btn">Book Now</a>
                                </li>
                                <li><a href="{{url('package_detail/'.$package_list->id)}}" class="link-btn">View Package</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--===== PLACES END ======-->
                @endforeach
                
            </div>
        </div>
    </section>
	<!-- ================ start footer Area ================= -->
    @include('partial.footer-car-theme') 
<!-- ================ End footer Area ================= -->
	<!--========= Scripts ===========-->
	<script src="{{url('core/js/car-theme/jquery-latest.min.js')}}"></script>
	<script src="{{url('core/js/car-theme/jquery-ui.js')}}"></script>
	<script src="{{url('core/js/car-theme/bootstrap.js')}}"></script>       
	<!--[if !(gte IE 8)]><!-->
            <script src="core/js/car-theme/wow.min.js')}}"></script>
            <script>
                // Initialize WOW
                //-------------------------------------------------------------
                new WOW({mobile: false}).init();
            </script>
        <!--<![endif]-->
	<script src="{{url('core/js/car-theme/materialize.min.js')}}"></script>
	<script src="{{url('core/js/car-theme/mail.js')}}"></script>
	<script src="{{url('core/js/car-theme/custom.js')}}"></script>
</body>
</html>