
<!DOCTYPE html>
<html lang="en">
@include('partial.header-car-theme')
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- MOBILE MENU -->
    <!--HEADER SECTION-->
    @include('partial.navigation-car-theme') 
    <!--END HEADER SECTION-->
	
	<section>
		<div class="rows inner_banner inner_banner_4">
			<div class="container">
				<h2><span>{{$package_detail['pack_name']}} -</span> Family Package</h2>
				<ul>
					<li><a href="#inner-page-title">Home</a>
					</li>
					<li><i class="fa fa-angle-right" aria-hidden="true"></i> </li>
					<li><a href="#inner-page-title" class="bread-acti">{{$package_detail['pack_name']}}</a>
					</li>
				</ul>
				<p>{{$package_detail['pack_short_inf']}}</p>
			</div>
		</div>
	</section>
    <section>
		<div class="rows banner_book" id="inner-page-title">
			<div class="container">
				<div class="banner_book_1">
					<ul>
						<li class="dl1">Location : {{$package_detail['pack_location']}}</li>
						<li class="dl2">Price : ₹ {{$package_detail['price']}}</li>
                        <?php $str_arr = explode (",", $package_detail['duration']);  ?>
						<li class="dl3">Duration : {{$str_arr[0]}} Nights/ {{$str_arr[1]}} Days</li>
						<li class="dl4"><a href="booking.html">Book Now</a> </li>
					</ul>
				</div>
			</div>
		</div>
	</section>
    <section>
		<div class="rows inn-page-bg com-colo">
			<div class="container inn-page-con-bg tb-space">
				<div class="col-md-9">
					<!--====== TOUR TITLE ==========-->
					<div class="tour_head">
						<h2>{{$package_detail['pack_name']}} <span class="tour_star"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i></span><span class="tour_rat">4.5</span></h2> </div>
					<!--====== TOUR DESCRIPTION ==========-->
					<div class="tour_head1">
						<h3>Description</h3>
						<p>{!! $package_detail['pack_long_desc'] !!}</p>
					</div>
					<!--====== ROOMS: HOTEL BOOKING ==========-->
					<div class="tour_head1 hotel-book-room">
						<h3>Photo Gallery</h3>
						<div id="myCarousel1" class="carousel slide" data-ride="carousel">
							<!-- Indicators -->
							<ol class="carousel-indicators carousel-indicators-1">
                            <?php $str_arr_images = explode (",", $package_detail['images']);  ?>
                            <?php $img=0;?>
                            @foreach($str_arr_images as $images)
                                <?php if($img == 0){$act = 'active';}else{$act = '';} ?>
								<li data-target="#myCarousel1" data-slide-to="{{$img}}" class="{{$act}}"><img src="{{url('core/images/car-theme/gallery/'.$images)}}" alt="Chania">
								</li>
                                <?php $img++;?>
                            @endforeach
								<!-- <li data-target="#myCarousel1" data-slide-to="1" class=""><img src="images/gallery/t2.jpg" alt="Chania">
								</li>
								<li data-target="#myCarousel1" data-slide-to="2" class=""><img src="images/gallery/t3.jpg" alt="Chania">
								</li>
								<li data-target="#myCarousel1" data-slide-to="3" class=""><img src="images/gallery/t4.jpg" alt="Chania">
								</li>
								<li data-target="#myCarousel1" data-slide-to="4" class="active"><img src="images/gallery/t5.jpg" alt="Chania">
								</li>
								<li data-target="#myCarousel1" data-slide-to="5" class=""><img src="images/gallery/s6.jpeg" alt="Chania">
								</li>
								<li data-target="#myCarousel1" data-slide-to="6" class=""><img src="images/gallery/s7.jpeg" alt="Chania">
								</li>
								<li data-target="#myCarousel1" data-slide-to="7" class=""><img src="images/gallery/s8.jpg" alt="Chania">
								</li>
								<li data-target="#myCarousel1" data-slide-to="8" class=""><img src="images/gallery/s9.jpg" alt="Chania">
								</li> -->
							</ol>
							<!-- Wrapper for slides -->
							<div class="carousel-inner carousel-inner1" role="listbox">
                            <?php $img1=0;?>
                            @foreach($str_arr_images as $images1)
                                <?php if($img1 == 0){$act = 'active';}else{$act = '';} ?>
								<div class="item {{$act}}"> <img src="{{url('core/images/car-theme/gallery/'.$images1)}}" alt="Chania" width="460" height="345"> </div>
								<?php $img1++;?>
                            @endforeach
								
							</div>
							<!-- Left and right controls -->
							<a class="left carousel-control" href="#myCarousel1" role="button" data-slide="prev"> <span><i class="fa fa-angle-left hotel-gal-arr" aria-hidden="true"></i></span> </a>
							<a class="right carousel-control" href="#myCarousel1" role="button" data-slide="next"> <span><i class="fa fa-angle-right hotel-gal-arr hotel-gal-arr1" aria-hidden="true"></i></span> </a>
						</div>
					</div>
					<!--====== TOUR LOCATION ==========-->
					<div class="tour_head1 tout-map map-container">
						<h3>Location</h3>
						<iframe src="{{$package_detail['map']}}" allowfullscreen=""></iframe>
					</div>
					<!--====== ABOUT THE TOUR ==========-->
					<div class="tour_head1">
						<h3>About The Tour</h3>
						<table>
							<tbody><tr>
								<th style="width:50%" class="event-res">Inclusions</th>
								<th class="event-res">Exclusions</th>
							</tr>
							<tr>
								<td class="event-res">{!! $package_detail['inclusion'] !!}</td>
								<td class="event-res">{!! $package_detail['exclusion'] !!} &amp; Taxes</td>
							</tr>
						</tbody></table>
					</div>
					<!--====== DURATION ==========-->
					<div class="tour_head1 l-info-pack-days days">
						<h3>Detailed Day Wise Itinerary</h3>
                        
						<ul>
                        <?php $i=1;?>
                        @foreach($day_wise_data as $dd)
                            <li class="l-info-pack-plac"> <i class="fa fa-clock-o" aria-hidden="true"></i>
								<h4><span>Day : {{$i}}</span> {{ $dd['day'] }}</h4>
								<p>{{ $dd['desc'] }}</p>
							</li>
                            <?php $i++;?>
                        @endforeach
							
						</ul>
					</div>
				</div>
				<div class="col-md-3 tour_r">
					<!--====== SPECIAL OFFERS ==========
					<div class="tour_right tour_offer">
						<div class="band1"><img src="images/offer.png" alt=""> </div>
						<p>Special Offer</p>
						<h4>$500<span class="n-td">
								<span class="n-td-1">$800</span>
								</span>
							</h4> <a href="booking.html" class="link-btn">Book Now</a> </div> -->
					<!--====== TRIP INFORMATION ==========-->
					<div class="tour_right tour_incl tour-ri-com">
						<h3>Trip Information</h3>
						<ul>
							<li>Location : {{$package_detail['pack_location']}}</li>
							<!-- <li>Arrival Date: Nov 12, 2017</li>
							<li>Departure Date: Nov 21, 2017</li>
							<li>Free Sightseeing &amp; Hotel</li> -->
						</ul>
					</div>
					<!--====== PACKAGE SHARE ==========-->
					<div class="tour_right head_right tour_social tour-ri-com">
						<h3>Share This Package</h3>
						<ul>
							<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
							<li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a> </li>
						</ul>
					</div>
					<!--====== HELP PACKAGE ==========-->
					<div class="tour_right head_right tour_help tour-ri-com">
						<h3>Help &amp; Support</h3>
						<div class="tour_help_1">
							<h4 class="tour_help_1_call">Call Us Now</h4>
							<h4><i class="fa fa-phone" aria-hidden="true"></i> +91-9998677923</h4> </div>
					</div>
					<!--====== PUPULAR TOUR PACKAGES ==========
					<div class="tour_right tour_rela tour-ri-com">
						<h3>Popular Packages</h3>
						<div class="tour_rela_1"> <img src="images/related1.png" alt="">
							<h4>Dubai 7Days / 6Nights</h4>
							<p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text</p> <a href="#" class="link-btn">View this Package</a> </div>
						<div class="tour_rela_1"> <img src="images/related2.png" alt="">
							<h4>Mauritius 4Days / 3Nights</h4>
							<p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text</p> <a href="#" class="link-btn">View this Package</a> </div>
						<div class="tour_rela_1"> <img src="images/related3.png" alt="">
							<h4>India 14Days / 13Nights</h4>
							<p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text</p> <a href="#" class="link-btn">View this Package</a> </div>
					</div>-->
				</div>
			</div>
		</div>
	</section>
	<!-- ================ start footer Area ================= -->
    @include('partial.footer-car-theme') 
<!-- ================ End footer Area ================= -->
	<!--========= Scripts ===========-->
	<script src="{{url('core/js/car-theme/jquery-latest.min.js')}}"></script>
	<script src="{{url('core/js/car-theme/jquery-ui.js')}}"></script>
	<script src="{{url('core/js/car-theme/bootstrap.js')}}"></script>       
	<!--[if !(gte IE 8)]><!-->
            <script src="core/js/car-theme/wow.min.js')}}"></script>
            <script>
                // Initialize WOW
                //-------------------------------------------------------------
                new WOW({mobile: false}).init();
            </script>
        <!--<![endif]-->
	<script src="{{url('core/js/car-theme/materialize.min.js')}}"></script>
	<script src="{{url('core/js/car-theme/mail.js')}}"></script>
	<script src="{{url('core/js/car-theme/custom.js')}}"></script>
</body>
</html>