<head>
    <title>Varun Travels Agency | Gujarat Car Rental Service</title>
    <!--== META TAGS ==-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- FAV ICON -->
    <link rel="shortcut icon" href="{{url('core/images/car-theme/varun_travels_favicon.png')}}">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:400,500,700" rel="stylesheet">
    <!-- FONT-AWESOME ICON CSS -->
    <link rel="stylesheet" href="{{url('core/css/car-theme/font-awesome.min.css')}}">
    <!--== ALL CSS FILES ==-->
    <link rel="stylesheet" href="{{url('core/css/car-theme/style.css')}}">
    <link rel="stylesheet" href="{{url('core/css/car-theme/materialize.css')}}">
    <link rel="stylesheet" href="{{url('core/css/car-theme/bootstrap.css')}}">
    <link rel="stylesheet" href="{{url('core/css/car-theme/mob.css')}}">
    <link rel="stylesheet" href="{{url('core/css/car-theme/animate.css')}}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
	<script src="core/js/html5shiv.js"></script>
	<script src="core/js/respond.min.js"></script>
	<![endif]-->
</head>