
<!DOCTYPE html>
<html lang="en">
@include('partial.header-car-theme')
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- MOBILE MENU -->
    <!--HEADER SECTION-->
    @include('partial.navigation-car-theme') 
    <!--END HEADER SECTION-->
	
	<!--DASHBOARD-->
	<section>
		<div class="rows contact-map map-container">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d235026.76898742886!2d72.70908998297934!3d23.013002006855324!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e85ea04102cb9%3A0xaf97daefbe139d3e!2sVarun%20Travels%20Agency!5e0!3m2!1sen!2sin!4v1616154344083!5m2!1sen!2sin"allowfullscreen=""></iframe>
		</div>
	</section>
  <section>
		<div class="form form-spac rows con-page">
			<div class="container">
				<!-- TITLE & DESCRIPTION -->
				<div class="spe-title col-md-12">
					<h2><span>Contact us</span></h2>
					<div class="title-line">
						<div class="tl-1"></div>
						<div class="tl-2"></div>
						<div class="tl-3"></div>
					</div>
					<p>Contact us on below details for more inquiry.</p>
				</div>

		<div class="pg-contact">
                        <div class="col-md-3 col-sm-6 col-xs-12 new-con new-con1"> <img src="img/contact/1.png" alt="">
                            <h4>Address</h4>
                            <p>19-B, Pragati Soc., Opp. Best School Rd. Bhairavnath, Maninagar - 380008
                            </p>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 new-con new-con3"> <img src="img/contact/2.png" alt="">
                            <h4>CONTACT INFO:</h4>
                            <p> <a href="tel://0099999999" class="contact-icon">Phone: +91 9998677923</a>
                                <br> <a href="tel://0099999999" class="contact-icon">Mobile: +91 9998677923</a>
                                <br> <a href="mailto:info@varuntravelsagency.com" class="contact-icon">Email: info@varuntravelsagency.com</a> </p>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 new-con new-con4"> <img src="img/contact/3.png" alt="">
                            <h4>Website</h4>
                            <p> <a href="#">Website: www.varuntravelsagency.com</a>
                                <br> <a href="#">Facebook: https://www.facebook.com/Varun-Travels-Agency-198449350225804/</a>
                        </div>
                    </div>				
			</div>
		</div>
	</section>

	<!-- ================ start footer Area ================= -->
    @include('partial.footer-car-theme') 
<!-- ================ End footer Area ================= -->
	<!--========= Scripts ===========-->
	<script src="core/js/car-theme/jquery-latest.min.js"></script>
	<script src="core/js/car-theme/jquery-ui.js"></script>
	<script src="core/js/car-theme/bootstrap.js"></script>       
	<!--[if !(gte IE 8)]><!-->
            <script src="core/js/car-theme/wow.min.js"></script>
            <script>
                // Initialize WOW
                //-------------------------------------------------------------
                new WOW({mobile: false}).init();
            </script>
        <!--<![endif]-->
	<script src="core/js/car-theme/materialize.min.js"></script>
	<script src="core/js/car-theme/mail.js"></script>
	<script src="core/js/car-theme/custom.js"></script>
    <script>
    $(document).ready(function(){
        $("#getquote").click(function(){
            debugger;
           $.ajax({
            url: "getquote",
            type: "POST",
            data: {
                "_token": "{{ csrf_token() }}",
                "from": $("#select-city").val(),
                "to": $("#select-city-1").val()
            },
            cache: false,
            success: function(dataResult) {
                console.log(dataResult);
                debugger;
                $(".v2-search-form").hide();
                $("#quoteresult").html(dataResult);
            }
        });
        });
    });
        
    </script>
</body>


<!-- Mirrored from rn53themes.net/themes/demo/travelz/booking-car-rentals.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 31 Dec 2019 22:02:49 GMT -->
</html>